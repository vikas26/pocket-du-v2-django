# Pocket DU API's (Version 2 - Django) #

This is created to manage the RDS database and hence Pocket Delhi University (Pocket DU) android application data using Admin panel of Django. It also serves few pages of Pocket DU via its API's.

### Live API's ###

1. [**Academic Calendar**](http://pocketdu.com:8001/du/api/v2.0.0/db/academic-calendar/)
2. [**Important Dates**](http://pocketdu.com:8001/du/api/v2.0.0/db/important-dates/)
3. [**Important Dates View**](http://pocketdu.com:8001/du/api/v2.0.0/db/important-dates-views/) - created this to get rid of port issue on Android and is served directed as a web view.