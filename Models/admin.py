from django.contrib import admin
from .models import AcademicCalendar, AccommodationsList,\
    AdministrationDetails, AmenitiesList,\
    ApiCallBlockData, ApiCallData,\
    CampusOfOpenLearning, CentresInstitutes,\
    CollegesList, Constants,\
    CoursesSyllabiCbcs, CoursesSyllabiFyup,\
    CutOffMarksList, CutOffPdf,\
    DuDetails, DuHomeImages,\
    DuHomeNews, FacultiesDepartments,\
    HelplineList, LibrariesList,\
    PhoneDirectory, SolrData,\
    ImportantDatesCategories, ImportantDates

# Models admin


class AcademicCalendarAdmin(admin.ModelAdmin):
    list_display = ["title", "posted_date", "status"]
    list_display_links = ["title", "posted_date"]
    list_editable = ["status"]
    list_filter = ["title", "status"]
    search_fields = ["title"]

    class Meta:
        model = AcademicCalendar


class AccommodationsListAdmin(admin.ModelAdmin):
    list_display = ["name", "status"]
    list_display_links = ["name"]
    list_editable = ["status"]
    list_filter = ["name", "status"]
    search_fields = ["name", "keywords"]

    class Meta:
        model = AcademicCalendar


class AdministrationDetailsAdmin(admin.ModelAdmin):
    list_display = ["designation", "name", "status"]
    list_display_links = ["designation", "name"]
    list_editable = ["status"]
    list_filter = ["designation", "name", "status"]
    search_fields = ["designation", "name"]

    class Meta:
        model = AdministrationDetails


class AmenitiesListAdmin(admin.ModelAdmin):
    list_display = ["name", "web_address", "status"]
    list_display_links = ["name", "web_address"]
    list_editable = ["status"]
    list_filter = ["name", "status"]
    search_fields = ["name", "keywords"]

    class Meta:
        model = AmenitiesList


class CampusOfOpenLearningAdmin(admin.ModelAdmin):
    list_display = ["name", "web_address", "status"]
    list_display_links = ["name", "web_address"]
    list_editable = ["status"]
    list_filter = ["name", "status"]
    search_fields = ["name", "description"]

    class Meta:
        model = CampusOfOpenLearning


class CentresInstitutesAdmin(admin.ModelAdmin):
    list_display = ["category_type", "institute_name", "web_address", "status"]
    list_display_links = ["category_type", "institute_name", "web_address"]
    list_editable = ["status"]
    list_filter = ["category_type", "institute_name", "status"]
    search_fields = ["category_type", "institute_name", "keywords"]

    class Meta:
        model = CentresInstitutes


class CollegesListAdmin(admin.ModelAdmin):
    list_display = ["college_name", "web_address", "status"]
    list_display_links = ["college_name", "web_address"]
    list_editable = ["status"]
    list_filter = ["college_name", "status"]
    search_fields = ["college_name", "keywords"]

    class Meta:
        model = CollegesList


class ConstantsAdmin(admin.ModelAdmin):
    list_display = ["name", "value", "description"]
    list_display_links = ["name", "description"]
    list_editable = ["value"]
    list_filter = ["name"]
    search_fields = ["name", "value", "description"]

    class Meta:
        model = Constants


class CoursesSyllabiCbcsAdmin(admin.ModelAdmin):
    list_display = ["title", "type", "faculty", "department", "status"]
    list_display_links = ["title", "type", "faculty", "department"]
    list_editable = ["status"]
    list_filter = ["type", "faculty", "department", "status"]
    search_fields = ["title", "faculty", "department"]

    class Meta:
        model = CoursesSyllabiCbcs


class CoursesSyllabiFyupAdmin(admin.ModelAdmin):
    list_display = ["title", "type", "language", "semester", "course_name", "status"]
    list_display_links = ["title", "type", "language", "semester", "course_name"]
    list_editable = ["status"]
    list_filter = ["title", "type", "language", "semester", "course_name"]
    search_fields = ["title", "type", "language", "semester", "course_name"]

    class Meta:
        model = CoursesSyllabiFyup


class CutOffMarksListAdmin(admin.ModelAdmin):
    list_display = ["stream_name", "college_name", "category_name", "course_name", "cut_off_marks", "cut_off_list_number", "year"]
    list_display_links = ["stream_name", "college_name", "category_name", "course_name", "cut_off_list_number", "year"]
    list_editable = ["cut_off_marks"]
    list_filter = ["stream_name", "college_name", "category_name", "course_name", "cut_off_list_number", "year"]
    search_fields = ["stream_name", "college_name", "category_name", "course_name"]

    class Meta:
        model = CutOffMarksList


class CutOffPdfAdmin(admin.ModelAdmin):
    list_display = ["title", "cut_off_number", "year", "status"]
    list_display_links = ["title", "cut_off_number", "year"]
    list_editable = ["status"]
    list_filter = ["title", "cut_off_number", "year", "status"]
    search_fields = ["title"]

    class Meta:
        model = CutOffPdf


class DuDetailsAdmin(admin.ModelAdmin):
    list_display = ["photo_url", "static_text"]
    list_display_links = ["photo_url", "static_text"]
    list_editable = []
    list_filter = []
    search_fields = []

    class Meta:
        model = DuDetails


class DuHomeImagesAdmin(admin.ModelAdmin):
    list_display = ["photo_url", "status"]
    list_display_links = ["photo_url"]
    list_editable = ["status"]
    list_filter = ["status"]
    search_fields = []

    class Meta:
        model = DuHomeImages


class DuHomeNewsAdmin(admin.ModelAdmin):
    list_display = ["title", "url", "posted_date", "status"]
    list_display_links = ["title", "url"]
    list_editable = ["posted_date", "status"]
    list_filter = ["posted_date", "created_time", "status"]
    search_fields = ["title"]

    class Meta:
        model = DuHomeNews


class FacultiesDepartmentsAdmin(admin.ModelAdmin):
    list_display = ["faculty_type", "faculty_name", "department_name", "status"]
    list_display_links = ["faculty_type", "faculty_name", "department_name"]
    list_editable = ["status"]
    list_filter = ["faculty_type", "faculty_name", "status"]
    search_fields = ["faculty_name", "department_name"]

    class Meta:
        model = FacultiesDepartments


class HelplineListAdmin(admin.ModelAdmin):
    list_display = ["name", "web_address", "email", "status"]
    list_display_links = ["name", "web_address", "email", "status"]
    list_editable = []
    list_filter = []
    search_fields = []

    class Meta:
        model = HelplineList


class LibrariesListAdmin(admin.ModelAdmin):
    list_display = ["name", "web_address", "email", "status"]
    list_display_links = ["name", "web_address", "email"]
    list_editable = ["status"]
    list_filter = ["status"]
    search_fields = ["name"]

    class Meta:
        model = LibrariesList


class PhoneDirectoryAdmin(admin.ModelAdmin):
    list_display = ["name", "url", "status"]
    list_display_links = ["name", "url"]
    list_editable = ["status"]
    list_filter = ["status"]
    search_fields = ["name"]

    class Meta:
        model = PhoneDirectory


class ImportantDatesAdmin(admin.ModelAdmin):
    list_display = ["title", "date", "publish"]
    list_display_links = ["title"]
    list_editable = ["date", "publish"]
    list_filter = ["title"]
    search_fields = ["title"]

    class Meta:
        model = ImportantDates


class ImportantDatesCategoriesAdmin(admin.ModelAdmin):
    list_display = ["category_name", "publish"]
    list_display_links = ["category_name"]
    list_editable = ["publish"]
    list_filter = ["category_name", "publish"]
    search_fields = ["category_name"]

    class Meta:
        model = ImportantDatesCategories


# Register your models here.
admin.site.register(AcademicCalendar, AcademicCalendarAdmin)
admin.site.register(AccommodationsList, AccommodationsListAdmin)
admin.site.register(AdministrationDetails, AdministrationDetailsAdmin)
admin.site.register(AmenitiesList, AmenitiesListAdmin)
admin.site.register(ApiCallBlockData)
admin.site.register(ApiCallData)
admin.site.register(CampusOfOpenLearning, CampusOfOpenLearningAdmin)
admin.site.register(CentresInstitutes, CentresInstitutesAdmin)
admin.site.register(CollegesList, CollegesListAdmin)
admin.site.register(Constants, ConstantsAdmin)
admin.site.register(CoursesSyllabiCbcs, CoursesSyllabiCbcsAdmin)
admin.site.register(CoursesSyllabiFyup, CoursesSyllabiFyupAdmin)
admin.site.register(CutOffMarksList, CutOffMarksListAdmin)
admin.site.register(CutOffPdf, CutOffPdfAdmin)
admin.site.register(DuDetails, DuDetailsAdmin)
admin.site.register(DuHomeImages, DuHomeImagesAdmin)
admin.site.register(DuHomeNews, DuHomeNewsAdmin)
admin.site.register(FacultiesDepartments, FacultiesDepartmentsAdmin)
admin.site.register(HelplineList, HelplineListAdmin)
admin.site.register(LibrariesList, LibrariesListAdmin)
admin.site.register(PhoneDirectory, PhoneDirectoryAdmin)
admin.site.register(SolrData)
admin.site.register(ImportantDatesCategories, ImportantDatesCategoriesAdmin)
admin.site.register(ImportantDates, ImportantDatesAdmin)
