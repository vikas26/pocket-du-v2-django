# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django
# to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table
# values or field names.
#
# Also note: You'll have to insert the output of 'django-admin
# sqlcustom [app_label]'
# into your database.

from __future__ import unicode_literals

from django.db import models


class AcademicCalendar(models.Model):
    title = models.TextField()
    url = models.TextField(blank=True, null=True)
    posted_date = models.DateField()
    attachment_type = models.IntegerField()
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'academic_calendar'


class AccommodationsList(models.Model):
    name = models.TextField()
    web_address = models.TextField(blank=True, null=True)
    email = models.TextField(blank=True, null=True)
    tel = models.TextField(blank=True, null=True)
    fax = models.TextField(blank=True, null=True)
    office_address = models.TextField(blank=True, null=True)
    type_flag = models.IntegerField(blank=True, null=True)
    location_coordinates = models.TextField(blank=True, null=True)
    keywords = models.TextField(blank=True, null=True)
    status = models.IntegerField()
    sort = models.IntegerField()
    description = models.TextField(blank=True, null=True)
    photo_url = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'accommodations_list'
        unique_together = (('type_flag', 'sort'),)


class AdministrationDetails(models.Model):
    sort = models.IntegerField(unique=True)
    designation = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    photo_url = models.TextField(blank=True, null=True)
    email = models.TextField(blank=True, null=True)
    tel = models.TextField(blank=True, null=True)
    fax = models.TextField(blank=True, null=True)
    office_address = models.TextField(blank=True, null=True)
    office_timings = models.TextField(blank=True, null=True)
    office_timing_day = models.TextField(blank=True, null=True)
    location_coordinates = models.TextField(blank=True, null=True)
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'administration_details'


class AmenitiesList(models.Model):
    name = models.TextField()
    web_address = models.TextField(blank=True, null=True)
    email = models.TextField(blank=True, null=True)
    tel = models.TextField(blank=True, null=True)
    fax = models.TextField(blank=True, null=True)
    office_address = models.TextField(blank=True, null=True)
    location_coordinates = models.TextField(blank=True, null=True)
    keywords = models.TextField(blank=True, null=True)
    status = models.IntegerField()
    sort = models.IntegerField(unique=True)

    class Meta:
        managed = False
        db_table = 'amenities_list'


class ApiCallBlockData(models.Model):
    created_time = models.DateTimeField(blank=True, null=True)
    request_header = models.TextField(blank=True, null=True)
    public_ip = models.CharField(max_length=100, blank=True, null=True)
    private_ip = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'api_call_block_data'


class ApiCallData(models.Model):
    created_time = models.DateTimeField(blank=True, null=True)
    get_from = models.CharField(max_length=100, blank=True, null=True)
    get_select = models.CharField(max_length=100, blank=True, null=True)
    get_where = models.CharField(max_length=100, blank=True, null=True)
    get_search = models.CharField(max_length=100, blank=True, null=True)
    get_type = models.CharField(max_length=100, blank=True, null=True)
    server_response = models.TextField(blank=True, null=True)
    request_header = models.TextField(blank=True, null=True)
    cut_off_college_id = models.CharField(
        max_length=100, blank=True, null=True)
    cut_off_stream_id = models.CharField(max_length=100, blank=True, null=True)
    cut_off_course_id = models.CharField(max_length=100, blank=True, null=True)
    cut_off_year = models.CharField(max_length=100, blank=True, null=True)
    cut_off_list_number = models.CharField(max_length=2, blank=True, null=True)
    cut_off_category_id = models.CharField(
        max_length=10, blank=True, null=True)
    cut_off_user_marks = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'api_call_data'


class CampusOfOpenLearning(models.Model):
    name = models.TextField()
    web_address = models.TextField(blank=True, null=True)
    location_coordinates = models.TextField(blank=True, null=True)
    keywords = models.TextField(blank=True, null=True)
    status = models.IntegerField()
    description = models.TextField(blank=True, null=True)
    photo_url = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'campus_of_open_learning'


class CentresInstitutes(models.Model):
    category_type = models.IntegerField()
    institute_name = models.TextField()
    photo_url = models.TextField(blank=True, null=True)
    web_address = models.TextField(blank=True, null=True)
    email = models.TextField(blank=True, null=True)
    tel = models.TextField(blank=True, null=True)
    fax = models.TextField(blank=True, null=True)
    office_address = models.TextField(blank=True, null=True)
    office_timings = models.TextField(blank=True, null=True)
    location_coordinates = models.TextField(blank=True, null=True)
    keywords = models.TextField(blank=True, null=True)
    status = models.IntegerField()
    sort = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'centres_institutes'
        unique_together = (('category_type', 'sort'),)


class CollegesList(models.Model):
    college_name = models.TextField()
    web_address = models.TextField(blank=True, null=True)
    email = models.TextField(blank=True, null=True)
    tel = models.TextField(blank=True, null=True)
    fax = models.TextField(blank=True, null=True)
    office_address = models.TextField(blank=True, null=True)
    location_coordinates = models.TextField(blank=True, null=True)
    keywords = models.TextField(blank=True, null=True)
    status = models.IntegerField()
    sort = models.IntegerField(unique=True)

    class Meta:
        managed = False
        db_table = 'colleges_list'


class Constants(models.Model):
    name = models.CharField(primary_key=True, max_length=100)
    value = models.CharField(max_length=100)
    description = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'constants'


class CoursesSyllabiCbcs(models.Model):
    title = models.TextField()
    type = models.IntegerField()
    faculty = models.TextField(blank=True, null=True)
    department = models.TextField(blank=True, null=True)
    department_type = models.IntegerField()
    course_name = models.TextField(blank=True, null=True)
    url = models.TextField(blank=True, null=True)
    attachment_type = models.IntegerField()
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'courses_syllabi_cbcs'


class CoursesSyllabiFyup(models.Model):
    title = models.TextField()
    type = models.IntegerField()
    language = models.TextField(blank=True, null=True)
    semester = models.TextField(blank=True, null=True)
    course_name = models.TextField(blank=True, null=True)
    paper_number = models.TextField(blank=True, null=True)
    url = models.TextField(blank=True, null=True)
    attachment_type = models.IntegerField()
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'courses_syllabi_fyup'


class CutOffMarksList(models.Model):
    stream_name = models.CharField(max_length=100, blank=True, null=True)
    college_name = models.CharField(max_length=200, blank=True, null=True)
    category_name = models.CharField(max_length=20, blank=True, null=True)
    course_name = models.CharField(max_length=100, blank=True, null=True)
    cut_off_marks = models.CharField(max_length=100, blank=True, null=True)
    cut_off_list_number = models.CharField(max_length=3, blank=True, null=True)
    year = models.CharField(max_length=50, blank=True, null=True)
    college_cut_off_url = models.TextField(blank=True, null=True)
    additional_info = models.CharField(max_length=300, blank=True, null=True)
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cut_off_marks_list'


class CutOffPdf(models.Model):
    title = models.CharField(max_length=255)
    cut_off_number = models.CharField(max_length=20)
    year = models.CharField(max_length=10)
    url = models.TextField(blank=True, null=True)
    posted_date = models.DateField()
    attachment_type = models.IntegerField()
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cut_off_pdf'


class DuDetails(models.Model):
    photo_url = models.TextField(blank=True, null=True)
    static_text = models.TextField(blank=True, null=True)
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'du_details'


class DuHomeImages(models.Model):
    photo_url = models.TextField(blank=True, null=True)
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'du_home_images'


class DuHomeNews(models.Model):
    title = models.TextField(blank=True, null=True)
    url = models.TextField(blank=True, null=True)
    sub_heading = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    posted_date = models.DateField()
    created_time = models.DateTimeField(blank=True, null=True)
    type = models.IntegerField()
    type_value = models.CharField(max_length=30)
    du_type = models.IntegerField()
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'du_home_news'


class FacultiesDepartments(models.Model):
    faculty_type = models.CharField(max_length=3)
    faculty_name = models.TextField()
    department_name = models.TextField(blank=True, null=True)
    photo_url = models.TextField(blank=True, null=True)
    web_address = models.TextField(blank=True, null=True)
    email = models.TextField(blank=True, null=True)
    tel = models.TextField(blank=True, null=True)
    fax = models.TextField(blank=True, null=True)
    office_address = models.TextField(blank=True, null=True)
    courses = models.TextField(blank=True, null=True)
    courses_link = models.TextField(blank=True, null=True)
    location_coordinates = models.TextField(blank=True, null=True)
    keywords = models.TextField(blank=True, null=True)
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'faculties_departments'


class HelplineList(models.Model):
    name = models.TextField(blank=True, null=True)
    web_address = models.TextField(blank=True, null=True)
    email = models.TextField(blank=True, null=True)
    tel = models.TextField(blank=True, null=True)
    fax = models.TextField(blank=True, null=True)
    office_address = models.TextField(blank=True, null=True)
    office_timings = models.TextField(blank=True, null=True)
    office_timing_day = models.TextField(blank=True, null=True)
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'helpline_list'


class LibrariesList(models.Model):
    name = models.TextField()
    web_address = models.TextField(blank=True, null=True)
    email = models.TextField(blank=True, null=True)
    tel = models.TextField(blank=True, null=True)
    fax = models.TextField(blank=True, null=True)
    office_address = models.TextField(blank=True, null=True)
    office_timings = models.TextField(blank=True, null=True)
    location_coordinates = models.TextField(blank=True, null=True)
    keywords = models.TextField(blank=True, null=True)
    status = models.IntegerField()
    sort = models.IntegerField(unique=True)

    class Meta:
        managed = False
        db_table = 'libraries_list'


class PhoneDirectory(models.Model):
    name = models.TextField()
    url = models.TextField(blank=True, null=True)
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'phone_directory'


class SolrData(models.Model):
    value_id = models.CharField(max_length=10, blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    keywords = models.TextField(blank=True, null=True)
    location_coordinates = models.TextField(blank=True, null=True)
    type = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'solr_data'


class ImportantDatesCategories(models.Model):
    category_name = models.CharField(max_length=255)
    publish = models.BooleanField(default=True)

    class Meta:
        managed = True
        db_table = 'important_dates_categories'

    def __unicode__(self):
        return self.category_name


class ImportantDates(models.Model):
    title = models.CharField(max_length=255, blank=True, null=True)
    category_id = models.ForeignKey(ImportantDatesCategories)
    date = models.DateTimeField()
    publish = models.BooleanField(default=False)

    class Meta:
        managed = True
        db_table = 'important_dates'
