# django level import
from django.db.models import F
from django.shortcuts import render, render_to_response
from django.views.generic import View
from Models.models import ImportantDates
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response

# python level import

# Create your views here.


def fix_dt(raw_date):
    """Replace 'midnight', 'noon', etc."""
    return raw_date
    return str(raw_date).replace('00:00:00+00:00', '')


class GetImportantDates(viewsets.ModelViewSet):

    def get_important_dates(self, request, *args, **kwargs):
        fields = [
            "title",
            "date"
        ]
        fields += ["category_name"]
        response = {
            'status_code': status.HTTP_200_OK,
            'error': [],
            'data': []
        }
        raw_response = ImportantDates.objects.\
            filter(
                publish=True,
                category_id__publish=True
            ).\
            annotate(
                category_name=F("category_id__category_name")
            ).\
            order_by(
                'category_id',
                'date'
            ).\
            values(*fields)

        modified_response = []
        key_to_array_number = {}
        for imp_date in raw_response:
            if imp_date['category_name'] not in key_to_array_number:
                index_number = len(modified_response)
                key_to_array_number[imp_date['category_name']] = index_number
                new_row = {}
                new_row["name"] = imp_date['category_name']
                imp_date.pop("category_name", None)
                imp_date['date'] = fix_dt(imp_date['date'])
                new_row["data"] = [imp_date]
                modified_response.append(new_row)
            else:
                index_number = key_to_array_number[imp_date['category_name']]
                imp_date.pop("category_name", None)
                imp_date['date'] = fix_dt(imp_date['date'])
                modified_response[index_number]['data'].append(imp_date)

        response['data'] = modified_response
        return Response(response)


class GetImportantDatesView(viewsets.ModelViewSet):
    def get_data(self, request, *args, **kwargs):
        template_name = 'important_dates.html'
        data = GetImportantDates().get_important_dates(self.request)
        return render_to_response(template_name, data.data)
