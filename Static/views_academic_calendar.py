# django level import
from Models.models import AcademicCalendar
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response

# python level import

# Create your views here.


class GetAcademicCalendar(viewsets.ModelViewSet):
    fields = [
        "id",
        "title",
        "url",
        "posted_date",
        "attachment_type"
    ]

    def get_academic_calendar(self, request, *args, **kwargs):
        response = {
            'status_code': status.HTTP_200_OK,
            'error': [],
            'data': []
        }
        response['data'] = AcademicCalendar.objects.\
            filter(
                status=1
            ).\
            values(*self.fields)
        return Response(response)
